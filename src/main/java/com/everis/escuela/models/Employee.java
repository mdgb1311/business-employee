package com.everis.escuela.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "employees")
public class Employee extends AuditModel {

	private static final long serialVersionUID = -8126953928951764760L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id = null;
	
	@Column(length = 50, name = "first_name", nullable = false)
	private String firstName = null;
	
	@Column(length = 50, name = "last_name", nullable = false)
	private String lastName = null;
	
	@Column(length = 11, name = "document_number", nullable = false)
	private String documentNumber = null;
	
	@Column(length = 20, name = "email")
	private String email = null;
}
