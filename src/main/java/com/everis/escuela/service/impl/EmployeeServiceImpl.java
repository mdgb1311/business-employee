package com.everis.escuela.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everis.escuela.models.Employee;
import com.everis.escuela.repository.EmployeeRepository;
import com.everis.escuela.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepo = null;
	
	@Override
	public void saveEmployee(Employee employee) {
		this.employeeRepo.save(employee);
	}

}
